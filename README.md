# Data Structures and Algorithms

> The Java **Data Structures** and **Algorithms** Explorer is a comprehensive learning project designed to delve deep
> into the world of data structures and algorithms using the Java programming language.
> This project serves as a practical, hands-on approach for understanding the fundamental
> concepts that form the backbone of computer science.

## Stack

Implement essential stack operations in Java push, pull, and pop. Gain insight into this fundamental data structure's
usage and applications with concise code examples. Ideal for solidifying Java skills and understanding foundational
concepts in computer science.

- [Array Bound Stack Implementation](app/src/main/java/com/vrapalis/www/stack/ArrayBoundStack.java) based on array as
  element holder.
- [Array List Stack Implementation](app/src/main/java/com/vrapalis/www/stack/ArrayListStack.java) based on array list as
  element holder.
- [Linked Stack Implementation](app/src/main/java/com/vrapalis/www/stack/LinkedStack.java) based on reference.
- [Balanced Algorithm](app/src/main/java/com/vrapalis/www/stack/BalancedStackUtility.java) determine valid expression (open,close parenthesis) by using stack as data-structure.
- [Postfix Evaluation Algorithm](app/src/main/java/com/vrapalis/www/stack/BalancedStackUtility.java) determine the value of post evaluation expression.

## Map

- [Word Count Algorithm](./app/src/main/java/com/vrapalis/www/map/algorithm/CountWordsUsingMapAlgorithm.java) count the word appearance in provided text.  

## Algorithms

- [Factorial Algorithm](app/src/main/java/com/vrapalis/www/algorithm/FactorAlgorithms.java) recursive and iterative algorithms to compute factorial of a given number.