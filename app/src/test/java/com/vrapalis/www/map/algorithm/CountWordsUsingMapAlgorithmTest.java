package com.vrapalis.www.map.algorithm;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Count words in text test")
class CountWordsUsingMapAlgorithmTest {

  @Test
  @DisplayName("text => 'apple banana mango apple apple' should count word apple three times test")
  void countWordsTest() {
    // Given
    var givenText = "apple banana mango apple apple";

    // When
    var resultMap = CountWordsUsingMapAlgorithm.countWordsAndSortByCounter(givenText);

    // Then
    Assertions.assertThat(resultMap).containsEntry("apple", 3);
  }
}