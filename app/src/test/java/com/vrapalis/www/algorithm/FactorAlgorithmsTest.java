package com.vrapalis.www.algorithm;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Factor algorithm test")
class FactorAlgorithmTest {

  @Test
  @DisplayName("Factor method (recursion) of 4 should return 24 test")
  void factorOf4ShouldReturn24RecursionTest() {
    // Given
    var factor = 4;

    // When
    int result = FactorAlgorithm.factorRecursion(4);

    // Then
    Assertions.assertEquals(24, result);
  }

  @Test
  @DisplayName("Factor method (iterative) of 4 should return 24 test")
  void factorOf4ShouldReturn24IterativeTest() {
    // Given
    var factor = 4;

    // When
    int result = FactorAlgorithm.factorIterative(4);

    // Then
    Assertions.assertEquals(24, result);
  }
}