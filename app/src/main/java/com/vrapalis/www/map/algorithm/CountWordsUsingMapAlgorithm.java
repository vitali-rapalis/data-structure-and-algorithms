package com.vrapalis.www.map.algorithm;

import java.util.HashMap;
import java.util.Map;

public final class CountWordsUsingMapAlgorithm {

  private CountWordsUsingMapAlgorithm() {
    throw new IllegalStateException("Utility class");
  }

  public static Map<String, Integer> countWordsAndSortByCounter(String text) {
    if (text == null || text.isBlank()) {
      throw new IllegalArgumentException("text cannot be null or blank");
    }

    var words = text.trim().toLowerCase().replaceAll("[^a-zA-Z0-9\\s]", "").split(" ");

    if (words.length < 2) {
      throw new IllegalArgumentException("text must contain at least two words");
    }

    Map<String, Integer> result = new HashMap<>();

    for (String word : words) {
      result.put(word, result.getOrDefault(word, 0) + 1);
    }

    return result;
  }
}
