package com.vrapalis.www.stack;

public final class ArrayBoundStack<T> implements Stack<T> {
  private int topIndex = -1;
  private T[] elements;

  public ArrayBoundStack() {
    elements = (T[]) new Object[DEFAULT_SIZE];
  }

  public ArrayBoundStack(int size) {
    elements = (T[]) new Object[size];
  }

  @Override
  public void push(T t) {
    if (isFull()) {
      throw new StackOverflowException();
    }

    elements[++topIndex] = t;
  }

  @Override
  public void pop() {
    if (isEmpty()) {
      throw new StackUnderflowException();
    }
    elements[topIndex--] = null;
  }

  @Override
  public T top() {
    if (isEmpty()) {
      throw new StackUnderflowException();
    }

    return elements[topIndex];
  }

  @Override
  public boolean isEmpty() {
    if (topIndex == -1) {
      return true;
    }

    return false;
  }

  @Override
  public boolean isFull() {
    if (size() == topIndex + 1) {
      return true;
    }
    return false;
  }

  @Override
  public int size() {
    return elements.length;
  }

  @Override
  public int itemsCount() {
    return topIndex + 1;
  }
}
