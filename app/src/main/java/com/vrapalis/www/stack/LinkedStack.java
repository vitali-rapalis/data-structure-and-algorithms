package com.vrapalis.www.stack;

import java.util.concurrent.atomic.AtomicInteger;

public final class LinkedStack<T> implements Stack<Node<T>> {

  private Node<T> top;
  private AtomicInteger next = new AtomicInteger(0);


  @Override
  public void push(Node<T> node) throws StackOverflowException {
    node.setLink(top);
    top = node;
    next.incrementAndGet();
  }

  @Override
  public void pop() throws StackUnderflowException {
    if (isEmpty()) {
      throw new StackUnderflowException();
    }

    var old = top;
    top = old.getLink();
    old.setLink(null);

    next.decrementAndGet();
  }

  @Override
  public Node<T> top() {
    return top;
  }

  @Override
  public boolean isEmpty() {
    if (top == null) {
      return true;
    }
    return false;
  }

  @Override
  public boolean isFull() {
    return false;
  }

  @Override
  public int size() {
    return next.get();
  }

  @Override
  public int itemsCount() {
    return next.get();
  }
}
