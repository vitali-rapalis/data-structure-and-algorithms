package com.vrapalis.www.stack;

public final class Node<T> {
  private T value;
  private Node<T> link;

  public Node(T value) {
    this.value = value;
  }

  public void setLink(Node<T> link) {
    this.link = link;
  }

  public Node<T> getLink() {
    return link;
  }

  public T getValue() {
    return value;
  }
}
