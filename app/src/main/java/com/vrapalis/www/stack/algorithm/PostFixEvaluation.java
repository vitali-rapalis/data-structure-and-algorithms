package com.vrapalis.www.stack.algorithm;

import com.vrapalis.www.stack.ArrayBoundStack;
import com.vrapalis.www.stack.StackUnderflowException;
import java.util.Scanner;

public final class PostFixEvaluation {

  private PostFixEvaluation() {
    throw new UnsupportedOperationException("Utility Class");
  }

  public static Integer evaluate(String expression) {
    var stack = new ArrayBoundStack<Integer>(expression.length());
    var scanner = new Scanner(expression);
    Integer operandOne = null;
    Integer operandTwo = null;

    while (scanner.hasNext()) {
      if (scanner.hasNextInt()) {
        var token = scanner.nextInt();

        if (stack.isFull()) {
          throw new StackOverflowError();
        }

        stack.push(token);
      } else {
        var token = scanner.next();

        if (!(token.equals("-") || token.equals("+") || token.equals("*") || token.equals("/"))) {
          throw new IllegalArgumentException();
        }

        if (stack.isEmpty()) {
          throw new StackUnderflowException();
        }

        operandOne = stack.top();
        stack.pop();

        if (stack.isEmpty()) {
          throw new StackUnderflowException();
        }

        operandTwo = stack.top();
        stack.pop();

        if (token.equals("+")) {
          stack.push(operandOne + operandTwo);
          continue;
        }

        if (token.equals("*")) {
          stack.push(operandOne * operandTwo);
          continue;
        }

        if (token.equals("/")) {
          stack.push(operandOne / operandTwo);
          continue;
        }

        if (token.equals("-")) {
          stack.push(operandOne - operandTwo);
        }
      }
    }

    return stack.top();
  }
}
