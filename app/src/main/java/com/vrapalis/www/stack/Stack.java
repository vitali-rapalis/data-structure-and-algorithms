package com.vrapalis.www.stack;

public interface Stack<T> {
  int DEFAULT_SIZE = 10;

  void push(T t) throws StackOverflowException;

  void pop() throws StackUnderflowException;

  T top();

  boolean isEmpty();

  boolean isFull();

  int size();

  int itemsCount();
}
