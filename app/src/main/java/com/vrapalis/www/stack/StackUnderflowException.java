package com.vrapalis.www.stack;

public final class StackUnderflowException extends RuntimeException {
  public StackUnderflowException() {
    super("Stack underflow");
  }
}
