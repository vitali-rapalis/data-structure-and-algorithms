package com.vrapalis.www.stack;

import java.util.ArrayList;
import java.util.List;

public final class ArrayListStack<T> implements Stack<T> {
  private List<T> elements;

  public ArrayListStack() {
    elements = new ArrayList<T>();
  }

  @Override
  public void push(T t) throws StackOverflowException {
    elements.add(t);
  }

  @Override
  public void pop() throws StackUnderflowException {
    if (isEmpty()) {
      throw new StackUnderflowException();
    }

    elements.remove(elements.size() - 1);
  }

  @Override
  public T top() {
    if (isEmpty()) {
      throw new StackUnderflowException();
    }

    return elements.get(elements.size() - 1);
  }

  @Override
  public boolean isEmpty() {
    if (elements.size() == 0) {
      return true;
    }
    return false;
  }

  @Override
  public boolean isFull() {
    return false;
  }

  @Override
  public int size() {
    return elements.size();
  }

  @Override
  public int itemsCount() {
    throw new UnsupportedOperationException();
  }
}
