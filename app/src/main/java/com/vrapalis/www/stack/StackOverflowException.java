package com.vrapalis.www.stack;

public final class StackOverflowException extends RuntimeException {
  public StackOverflowException() {
    super("Stack overflow");
  }
}
