package com.vrapalis.www.stack.algorithm;

import com.vrapalis.www.stack.ArrayBoundStack;

public final class BalancedStackUtility {

  private BalancedStackUtility() {
    throw new UnsupportedOperationException();
  }


  public static boolean test(final String expression, final String openTags,
                             final String closingTags) {
    if (expression == null || expression.length() == 0) {
      return false;
    }

    if (openTags == null || openTags.length() == 0 || closingTags == null ||
        closingTags.length() == 0) {
      return true;
    }

    if (openTags.length() != closingTags.length()) {
      return false;
    }

    var isBalanced = true;
    var stack = new ArrayBoundStack<Integer>(expression.length());

    for (int i = 0; i < expression.length(); i++) {
      char processedChar = expression.charAt(i);

      if (openTags.contains(String.valueOf(processedChar))) {
        stack.push(openTags.indexOf(String.valueOf(processedChar)));
        continue;
      }

      if (closingTags.contains(String.valueOf(processedChar))) {
        var lastOpenTagIndex = stack.top();
        var closingTag = closingTags.indexOf(processedChar);

        if (!lastOpenTagIndex.equals(closingTag)) {
          isBalanced = false;
          continue;
        }

        stack.pop();
      }
    }

    if (!stack.isEmpty()) {
      isBalanced = false;
    }

    return isBalanced;
  }
}
