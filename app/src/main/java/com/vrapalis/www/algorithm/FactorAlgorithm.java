package com.vrapalis.www.algorithm;

public final class FactorAlgorithm {

  private FactorAlgorithm() {
    throw new UnsupportedOperationException();
  }

  public static int factorRecursion(int n) {
    if (n == 0) {
      return 1;
    }
    return n * factorRecursion(n - 1);
  }

  public static int factorIterative(int n) {
    var result = 1;
    while (!(n == 0)) {
      result = n * result;
      n--;
    }
    return result;
  }
}
